#include <iostream>
using namespace std;

int main()
{
    string fname;
    cout << "Enter your first name: " << endl;
     cin >> fname;

     string lname;
     cout << "Enter your last name: " << endl;
     cin >> lname;

     cout << "Hello, " << fname << " " << lname << endl;

     return 0;
}
